import { by } from 'protractor';
import { Component, Input, OnInit } from '@angular/core';
import { Crib } from './../crib';

const { version: cribVersion, name: appName } = require('../../../package.json');



@Component({
  selector: 'app-crib-card',
  templateUrl: './crib-card.component.html',
  styleUrls: ['./crib-card.component.css']
})
export class CribCardComponent implements OnInit {

  @Input('crib') crib: Crib;

  version: any;
  name: any;

  constructor() {
  }

  ngOnInit() {
    console.log('Application Name:', appName, 'Version:', cribVersion);
  }

}
